using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
    [SerializeField]
    private Transform target; // 따라다닐 타겟
    [SerializeField]
    private float distance = 10.0f; // x - z 평면상에서 타겟까지의 거리
    [SerializeField]
    private float height = 5.0f; // 카메라의 높이 값
    [SerializeField]
    private float rotationDamping; // 로테이션 감쇠
    [SerializeField]
    private float heightDamping; // 높이 감쇠

    private void LateUpdate()
    {
        //타겟이 없으면 종료
        if (!target)
            return;

        var wantedRotationAngle = target.eulerAngles.y; // 현재 회전각도 계산
        var wantedHeight = target.position.y + height; // 현재 높이 값
        var currentRotationAngle = this.transform.eulerAngles.y;
        var currentHeight = this.transform.position.y;

        // Mathf.LerpAngle(float a, float b, float t) = t시간동안 a 부터 b 까지 변경되는 각도를 변환함, 부드러운 회전을 구할때 쓰는 함수
        // y - 축을 기준으로 회전각도 계산
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

        // 이동 높이 계산
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

        // 오일러각을 쿼터니언으로 변환
        var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

        // x - z 평면상에서 카메라의 위치를 타겟의 뒷쪽에서 일정거리 떨어진 위치로 설정
        this.transform.position = target.position;
        this.transform.position -= (currentRotation * Vector3.forward) * distance;
        // 쿼터니언 * 벡터 Q * V 

        // 카메라 높이 설정
        this.transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

        // 항상 타겟을 바라보기
        this.transform.LookAt(target);
    }
}