using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveBullet : MonoBehaviour
{
    public GameObject sparkEffect; // 스파크 프리팹을 저장할 변수

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.collider.tag == "BULLET")
        {
            showEffect(coll); // 스파크 효과 함수
            Destroy(coll.gameObject);
        }
    }

    void showEffect(Collision coll)
    {
        /*
        coll.collider // 충돌한 게임 오브젝트이 Collider 컴포넌트
        coll.contacts // 물체 간의 충돌지점으로 물리엔진에 의해 생성
            - normal // 직각을 이루는 벡터 (법선)
            - otherCollider
            - point // 컨텍스트 정보에 위치
            - separation // 충돌한 두 Collider 간의 거리
            - thisCollider
        */

        ContactPoint contact = coll.contacts[0];

        // Quaternion.FromToRotation
        // From 방향 벡터를 to 방향으로 회전한 쿼터니언을 반환한다.
        // (중심축) -> (회전하고 싶은 방향 벡터)
        Quaternion rot = Quaternion.FromToRotation(-Vector3.forward, contact.normal);
        // Quaternion rot = Quaternion.LookRotation(-contact.normal);
        // 스파크 효과를 생성
        GameObject spark = Instantiate(sparkEffect, contact.point + (-contact.normal * 0.05f), rot);
        //Instantiate(sparkEffect, contact.point, rot);
        // 스파크 효과의 부모를 드럼통 또는 벽으로 설정
        spark.transform.SetParent(this.transform);
    }
}
