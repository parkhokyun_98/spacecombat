using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelCtrl : MonoBehaviour
{
    public GameObject expEffect; // 폭발 효과 프리팹을 저장할 변수
    public Mesh[] meshes;      // 찌그러진 드럼통의 메쉬를 저장할 배열
    public Texture[] textures; // 드럼통의 텍스처를 저장할 배열

    private int hitCount = 0; // 총알이 맞은 횟수
    private Rigidbody rb; // RigidBody 컴포넌트를 저장할 변수
    private MeshFilter meshFilter; // MeshFilter 컴포넌트를 저장할 배열
    private MeshCollider meshCollider; // MeshCollider 컴포넌트를 저장할 변수
    private MeshRenderer _renderer; // MeshRenderer 컴포넌트를 저장할 변수
    private AudioSource _audio; // AudioSource 컴포넌트를 저장할 변수

    public float expRadius = 10.0f; // 폭발반경
    public AudioClip expSfx; // 폭발음 오디오 클립

    public Shake shake;

    void Start()
    {
        rb = GetComponent<Rigidbody>(); // Rigidbody 컴포넌트를 추출해 저장
        meshFilter = GetComponent<MeshFilter>(); // MeshFilter 컴포넌트를 추출해 저장
        meshCollider = GetComponent<MeshCollider>(); // MeshCollider 컴포넌트를 추출해 저장
        _renderer = GetComponent<MeshRenderer>(); // MeshRenderer 컴포넌트를 추출해 저장

        // 난수를 발생시켜 불규칙적인 텍스처를 적용
        _renderer.material.mainTexture = textures[Random.Range(0, textures.Length)];

        // AudioSource 컴포넌트를 추출해 저장
        _audio = GetComponent<AudioSource>();

        shake = GameObject.Find("CameraRig").GetComponent<Shake>();
    }

    // 충돌이 발생했을 때 한 번 호출되는 콜백 함수
    private void OnCollisionEnter(Collision coll)
    {
        // 충돌한 게임오브젝트의 태그를 비교
        if(coll.collider.CompareTag("BULLET"))
        {
            // 총알의 충돌 횟수를 증가시키고 3발 이상 맞았는지 확인
            if(++hitCount == 3)
            {
                ExpBarrel();
            }
        }
    }

    // 폭발 효과를 처리할 변수
    void ExpBarrel()
    {
        // 폭발 효과 프리팹을 동적으로 생성
        GameObject effect = Instantiate(expEffect, transform.position, Quaternion.identity);
        Destroy(effect, 2.0f);
        // RIgidbody 컴포넌트 mass를 1.0으로 수정해 무게를 가볍게 함
        //rb.mass = 1.0f;
        // 위로 솟구치는 힘을 가함
        //rb.AddForce(Vector3.up * 1000.0f);

        IndirectDamage(transform.position); // 폭발력 생성(광역데미지)

        // 난수를 생성
        int idx = Random.Range(0, meshes.Length);
        // 찌그러진 메쉬를 적용
        meshFilter.sharedMesh = meshes[idx];

        meshCollider.sharedMesh = meshes[idx];

        // 폭발음 실행
        _audio.PlayOneShot(expSfx, 1.0f);

        StartCoroutine(shake.ShakeCamera(0.1f, 0.2f, 0.5f));
    }

    // 폭발력을 주변에 전달하는 함수
    void IndirectDamage(Vector3 pos)
    {
        /*
         * Physics.OverlapSphere (원점, 반지름, 레이어)
         */
        // 주변에 있는 드럼통을 모두 추출
        // LayerMask mask = LayerMask.GetMask("BALLER"); <ㅡ 시프트 쓰고싶지 않으면이렇게 가져와서 넣으면된다
        // int layer = 1 << LayerMask.NameToLayer("BALLER"); <ㅡ 2번째 방법 이렇게하고 layer를 넣으면 된다.
        Collider[] colls = Physics.OverlapSphere(pos, expRadius, 1 << 8); // 8번째 레이어

        foreach(var coll in colls)
        {
            // 폭발 범위에 포함된 드럼통의 Rigidbody 컴포넌트를 추출
            var _rb = coll.GetComponent<Rigidbody>();
            // 드럼통의 무게를 가볍게 함
            _rb.mass = 1.0f;
            // 폭발력을 전달
            _rb.AddExplosionForce(1200.0f, pos, expRadius, 1000.0f);
            /*
             *  Rigidbody.AddExplosionForce (횡 폭발력, 폭발원점, 폭발반경, 종 폭발력)
             */
        }
    }
}
/*
 * 빈번히 발생, 파일크기가 작은 경우 -> Decopress On Load, PCM 또는 APCM
 * 빈번히 발생, 파일크기가 중간 경우 -> Compressed in Memory, ADPCM
 * 가끔 발생, 파일크기가 작은 경우 -> Compressed in Memory, ADPCM
 * 가끔 발생, 파일크기가 중간인 경우 -> Compressed in Memory), Vorbis
 */