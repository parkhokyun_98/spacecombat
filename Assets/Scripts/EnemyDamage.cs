using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    private const string bulletTag = "BULLET";
    private float hp = 100.0f; // 생명 게이지
    private GameObject bloodEffect; // 피격 시 사용할 혈흔 효과

    void Start()
    {
        // 혈흔 효과 프리팹을 로드
        bloodEffect = Resources.Load<GameObject>("BulletImpactFleshBigEffect");

        /*
         * ex) Resources/Prefabs (리소스안 다른폴더 에서 AA찾기)
         * bloodEffect = Resources.Load<GameObject>("Prefabs/AA");
         * 
         * ex) Resources/Sounds (오디오 찾기)
         * sound = Resources.Load<AudioClip>("Sounds/gunFire");
         */
    }

    private void OnCollisionEnter(Collision coll)
    {
        if(coll.collider.tag == bulletTag)
        {
            ShowBloodEffect(coll);
            Destroy(coll.gameObject);

            hp -= coll.gameObject.GetComponent<BulletCtrl>().damage;
            if(hp <= 0.0f)
            {
                GetComponent<EnemyAI>().state = EnemyAI.State.DIE;
            }
        }
    }

    void ShowBloodEffect(Collision coll)
    {
        Vector3 pos = coll.contacts[0].point;
        Vector3 _normal = coll.contacts[0].normal;
        Quaternion rot = Quaternion.FromToRotation(-Vector3.forward, _normal);

        GameObject blood = Instantiate<GameObject>(bloodEffect, pos, rot);
        Destroy(blood, 1.0f);
    }
}