using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCtrl : MonoBehaviour
{
    public float damage = 20.0f; // 총알의 파괴력
    public float speed = 1000.0f; // 총알 발사 속도

    void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * speed); // 해당 게임 로컬좌표를 할경우)
        // AddRelativeForce(Vector3.forward * speed); // 이것도 로컬축이다 위에 코드랑 똑같다
    }
}