using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFire : MonoBehaviour
{
    private AudioSource audio; // AudioSource 컴포넌트를 저장할 변수
    private Animator animator; // Animator 컴포넌트를 저장할 변수
    private Transform playerTr; // 주인공 캐릭터의 Transform Component를 저장할 변수
    private Transform enemyTr; // 적 캐릭터의 Transform Component를 저장할 변수

    // 애니메이터 컨트롤러에 정의한 파라미터의 해시값을 미리 추출 
    private readonly int hashFire = Animator.StringToHash("Fire");
    private readonly int hashReload = Animator.StringToHash("Reload");

    private float nextFire = 0.0f; // 다음 발사할 시간 계산용 변수
    private readonly float fireRate = 0.1f; // 총알 발사 간격
    private readonly float damping = 10.0f; // 주인골을 향해 회전할 속도 계수

    private readonly float reloadTime = 2.0f; // 재장전 시간
    private readonly int maxBullet = 10; // 탄창의 최대 총알 수
    private int currBullet = 10; // 초기 총알 수
    private bool isReload = false; // 재장전 여부
    private WaitForSeconds wsReload; // 재장전 시간 동안 기다릴 변수 선언

    public bool isFire = false; // 총알 발사 여부를 판단할 변수
    public AudioClip fireSfx; // 총알 발사 사운드를 저장할 변수
    public AudioClip reloadSfx; // 재장전 사운드를 저장할 변수

    public GameObject Bullet; // 적 캐릭터의 총알 프리팹
    public Transform firePos; // 적 캐릭터의 총알 발사 위치
    public MeshRenderer muzzleFlash; // MuzzleFlash의 MeshRenderer 컴포넌트를 저장할 변수

    private void Start()
    {
        playerTr = GameObject.FindGameObjectWithTag("PLAYER").GetComponent<Transform>();
        enemyTr = GetComponent<Transform>();
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        wsReload = new WaitForSeconds(reloadTime);
        muzzleFlash.enabled = false; // MuzzleFlash를 비활성화
    }

    private void Update()
    {
        if(!isReload && isFire)
        {
            if(Time.time >= nextFire)
            {
                Fire();
                nextFire = Time.time + fireRate + Random.Range(0.0f, 0.3f);
            }

            Quaternion rot = Quaternion.LookRotation(playerTr.position - enemyTr.position);
            enemyTr.rotation = Quaternion.Slerp(enemyTr.rotation, rot, Time.deltaTime * damping);
        }
    }

    void Fire()
    {
        animator.SetTrigger(hashFire);
        audio.PlayOneShot(fireSfx, 1.0f);
        // 총구 화염 효과 코루틴 호출
        StartCoroutine(ShowMuzzleFlash());

        // 총알을 생성
        GameObject _bullet = Instantiate(Bullet, firePos.position, firePos.rotation);
        // 일정 시간이 지나면 삭제
        Destroy(_bullet, 3.0f);

        // 남은 총알로 재장전 여부를 계산
        isReload = (--currBullet % maxBullet == 0);
        if(isReload)
        {
            StartCoroutine(Reloading());
        }
    }

    // 나중에 용이 날아서 뭐하는거가 이 예시이다
    IEnumerator ShowMuzzleFlash()
    {
        // MuzzleFlash 활성화
        muzzleFlash.enabled = true;

        // 불규칙한 회전 각도를 계산
        Quaternion rot = Quaternion.Euler(Vector3.forward * Random.Range(0, 360));
        muzzleFlash.transform.localRotation = rot;
        muzzleFlash.transform.localScale = Vector3.one * Random.Range(1.0f, 2.0f);

        // 텍스처의 Offset 속성에 적용할 불규칙한 값을 생성
        Vector2 offset = new Vector2(Random.Range(0, 2), Random.Range(0, 2)) * 0.5f;
        // MuzzleFlash의 머테리얼의 Offset 값을 적용
        /*
         * SetTextureOffset
         *  텍스처의 오프셋 값을 수정할 때 사용한다
         *  
         *  _MainTex Diffuse
         *  _BumpMap Normal map
         *  _Cup Cubmap
         */
        muzzleFlash.material.SetTextureOffset("_MainTex", offset);

        // MuzzleFlash가 보일 동안 잠시 대기
        yield return new WaitForSeconds(Random.Range(0.05f, 0.2f));
        // MuzzleFlash를 다시 비활성화
        muzzleFlash.enabled = false;
    }

    IEnumerator Reloading()
    {
        // MuzzleFlash를 비활성화
        muzzleFlash.enabled = false;
         
        // 재장전 애니메이션 실행
        animator.SetTrigger(hashReload);
        // 재장전 사운드 재생
        audio.PlayOneShot(reloadSfx, 1.0f);

        // 재장전 시간만큼 대기하는 동안 제어권 양보
        yield return wsReload;

        // 총알의 계수를 초기화
        currBullet = maxBullet;
        isReload = false;
    }
}